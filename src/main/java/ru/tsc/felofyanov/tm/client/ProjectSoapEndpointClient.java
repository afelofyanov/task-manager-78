package ru.tsc.felofyanov.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.endpoint.IProjectEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class ProjectSoapEndpointClient {

    public static IProjectEndpoint getInstance(@NotNull final String baseURL) throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/services/ProjectEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "ProjectEndpointImplService";
        @NotNull final String ns = "http://endpoint.tm.felofyanov.tsc.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final IProjectEndpoint result = Service.create(url, name).getPort(IProjectEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }
}

