package ru.tsc.felofyanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.api.repository.ITaskRepository;
import ru.tsc.felofyanov.tm.api.service.ITaskService;
import ru.tsc.felofyanov.tm.exception.IdEmptyException;
import ru.tsc.felofyanov.tm.exception.ModelEmptyException;
import ru.tsc.felofyanov.tm.exception.UserIdEmptyException;
import ru.tsc.felofyanov.tm.model.Task;

import java.util.List;

@Service
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @Override
    @Transactional
    public void create(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Task task = new Task(
                userId,
                "New task: " + System.currentTimeMillis(),
                "Description " + System.currentTimeMillis());
        repository.save(task);
    }

    @Override
    @Transactional
    public Task save(@Nullable final String userId, @Nullable final Task task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new ModelEmptyException();
        task.setUserId(userId);
        return repository.save(task);
    }

    @Override
    @Transactional
    public List<Task> save(@Nullable final String userId, @Nullable final List<Task> tasks) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (tasks == null) throw new ModelEmptyException();
        for (Task task : tasks) {
            task.setUserId(userId);
            repository.save(task);
        }
        return tasks;
    }

    @Override
    @Transactional
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllByUserId(userId);
    }

    @Override
    @Transactional
    public Task findFirstByUserIdAndId(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeByByUserIdAndId(@Nullable final String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeByUserIdAndEntity(@Nullable final String userId, @Nullable final Task task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new ModelEmptyException();
        repository.deleteByUserIdAndId(userId, task.getId());
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final List<Task> tasks) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (tasks == null) throw new ModelEmptyException();
        tasks
                .stream()
                .forEach(task -> removeByUserIdAndEntity(userId, task));
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return repository.findFirstByUserIdAndId(userId, id) != null;
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @Override
    @Transactional
    public long count(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.countByUserId(userId);
    }
}
