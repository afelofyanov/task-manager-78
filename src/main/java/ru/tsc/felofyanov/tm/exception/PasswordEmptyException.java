package ru.tsc.felofyanov.tm.exception;

public final class PasswordEmptyException extends AbstractException {
    public PasswordEmptyException() {
        super("Error! Password is empty...");
    }
}
