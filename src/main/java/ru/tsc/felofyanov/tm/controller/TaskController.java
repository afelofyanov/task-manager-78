package ru.tsc.felofyanov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.felofyanov.tm.enumerated.Status;
import ru.tsc.felofyanov.tm.model.CustomUser;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.service.ProjectService;
import ru.tsc.felofyanov.tm.service.TaskService;

import java.util.Collection;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @GetMapping("/task/create")
    public String create(@AuthenticationPrincipal CustomUser user) {
        taskService.create(user.getUserId());
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @AuthenticationPrincipal CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.removeByByUserIdAndId(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @AuthenticationPrincipal CustomUser user,
            @ModelAttribute("task") Task task,
            BindingResult result) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.save(user.getUserId(), task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal CustomUser user,
            @PathVariable("id") String id
    ) {
        final Task task = taskService.findFirstByUserIdAndId(user.getUserId(), id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects(user));
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    private Collection<Project> getProjects(@AuthenticationPrincipal CustomUser user) {
        return projectService.findAll(user.getUserId());
    }

    public Status[] getStatuses() {
        return Status.values();
    }
}
