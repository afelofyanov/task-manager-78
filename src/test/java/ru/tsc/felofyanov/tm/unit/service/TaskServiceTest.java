package ru.tsc.felofyanov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.marker.BootUnitCategory;
import ru.tsc.felofyanov.tm.model.Task;
import ru.tsc.felofyanov.tm.service.TaskService;
import ru.tsc.felofyanov.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.List;

@Transactional
@SpringBootTest
@WebAppConfiguration
@Category(BootUnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class TaskServiceTest {

    @NotNull
    private final Task alpha = new Task("Test-1");
    @NotNull
    private final Task beta = new Task("Test-2");
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private TaskService service;

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        service.save(UserUtil.getUserId(), alpha);
        service.save(UserUtil.getUserId(), beta);
    }

    @After
    public void clean() {
        service.clear(UserUtil.getUserId());
    }

    @Test
    public void createTest() {
        Assert.assertEquals(2, service.count(UserUtil.getUserId()));
        service.create(UserUtil.getUserId());
        Assert.assertEquals(3, service.count(UserUtil.getUserId()));
    }

    @Test
    public void saveTest() {
        @Nullable final Task test = new Task("Tester");
        Assert.assertNotNull(test);
        service.save(UserUtil.getUserId(), test);
        Assert.assertNotNull(service.findFirstByUserIdAndId(UserUtil.getUserId(), test.getId()));
        Assert.assertEquals(3, service.count(UserUtil.getUserId()));
    }

    @Test
    public void saveAllTest() {
        @Nullable final List<Task> test = new ArrayList<>();
        @Nullable final Task beforeTask = new Task("Tester1");
        @Nullable final Task beforeTest = new Task("Tester2");

        test.add(beforeTask);
        test.add(beforeTest);

        Assert.assertNotNull(test);
        service.save(UserUtil.getUserId(), test);

        Assert.assertNotNull(service.findAll(UserUtil.getUserId()));
        Assert.assertEquals(4, service.count(UserUtil.getUserId()));

        @Nullable final Task afterTask = service.findFirstByUserIdAndId(UserUtil.getUserId(), beforeTask.getId());
        @Nullable final Task afterTest = service.findFirstByUserIdAndId(UserUtil.getUserId(), beforeTest.getId());

        Assert.assertNotNull(afterTask);
        Assert.assertNotNull(afterTest);

        Assert.assertEquals(afterTask.getId(), beforeTask.getId());
        Assert.assertEquals(afterTest.getId(), beforeTest.getId());
    }

    @Test
    public void findAllTest() {
        Assert.assertNotNull(service.findAll(UserUtil.getUserId()));
        Assert.assertEquals(2, service.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void findFirstByUserIdAndIdTest() {
        Assert.assertNotNull(service.findFirstByUserIdAndId(UserUtil.getUserId(), alpha.getId()));
        @Nullable final Task test = service.findFirstByUserIdAndId(UserUtil.getUserId(), alpha.getId());
        Assert.assertNotNull(test);
        Assert.assertEquals(alpha.getId(), test.getId());
    }

    @Test
    public void deleteByUserIdTest() {
        service.clear(UserUtil.getUserId());
        Assert.assertEquals(0, service.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void removeTest() {
        @Nullable final List<Task> test = new ArrayList<>();
        test.add(alpha);
        test.add(beta);

        Assert.assertNotNull(test);
        service.remove(UserUtil.getUserId(), test);
        Assert.assertEquals(0, service.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void removeByByUserIdAndIdTest() {
        service.removeByByUserIdAndId(UserUtil.getUserId(), alpha.getId());
        Assert.assertNull(service.findFirstByUserIdAndId(UserUtil.getUserId(), alpha.getId()));
        Assert.assertEquals(1, service.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void removeByUserIdAndEntityTest() {
        service.removeByUserIdAndEntity(UserUtil.getUserId(), alpha);
        Assert.assertNull(service.findFirstByUserIdAndId(UserUtil.getUserId(), alpha.getId()));
        Assert.assertEquals(1, service.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(2, service.count(UserUtil.getUserId()));
    }

    @Test
    public void existsByIdTest() {
        Assert.assertTrue(service.existsById(UserUtil.getUserId(), alpha.getId()));
        Assert.assertFalse(service.existsById("123qe", alpha.getId()));
    }
}
