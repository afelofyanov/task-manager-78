package ru.tsc.felofyanov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.marker.BootUnitCategory;
import ru.tsc.felofyanov.tm.model.Project;
import ru.tsc.felofyanov.tm.service.ProjectService;
import ru.tsc.felofyanov.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.List;

@Transactional
@SpringBootTest
@WebAppConfiguration

@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectServiceTest {

    @NotNull
    private final Project alpha = new Project("Test-1");
    @NotNull
    private final Project beta = new Project("Test-2");
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private ProjectService service;

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        service.save(UserUtil.getUserId(), alpha);
        service.save(UserUtil.getUserId(), beta);
    }

    @After
    public void clean() {
        service.clear(UserUtil.getUserId());
    }

    @Test
    @Category(BootUnitCategory.class)
    public void createTest() {
        Assert.assertEquals(2, service.count(UserUtil.getUserId()));
        service.create(UserUtil.getUserId());
        Assert.assertEquals(3, service.count(UserUtil.getUserId()));
    }

    @Test
    public void saveTest() {
        @Nullable final Project test = new Project("Tester");
        Assert.assertNotNull(test);
        service.save(UserUtil.getUserId(), test);
        Assert.assertNotNull(service.findFirstByUserIdAndId(UserUtil.getUserId(), test.getId()));
        Assert.assertEquals(3, service.count(UserUtil.getUserId()));
    }

    @Test
    public void saveAllTest() {
        @Nullable final List<Project> test = new ArrayList<>();
        @Nullable final Project beforeTask = new Project("Tester1");
        @Nullable final Project beforeTest = new Project("Tester2");

        test.add(beforeTask);
        test.add(beforeTest);

        Assert.assertNotNull(test);
        service.save(UserUtil.getUserId(), test);

        Assert.assertNotNull(service.findAll(UserUtil.getUserId()));
        Assert.assertEquals(4, service.count(UserUtil.getUserId()));

        @Nullable final Project afterTask = service.findFirstByUserIdAndId(UserUtil.getUserId(), beforeTask.getId());
        @Nullable final Project afterTest = service.findFirstByUserIdAndId(UserUtil.getUserId(), beforeTest.getId());

        Assert.assertNotNull(afterTask);
        Assert.assertNotNull(afterTest);

        Assert.assertEquals(afterTask.getId(), beforeTask.getId());
        Assert.assertEquals(afterTest.getId(), beforeTest.getId());
    }

    @Test
    public void findAllTest() {
        Assert.assertNotNull(service.findAll(UserUtil.getUserId()));
        Assert.assertEquals(2, service.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void findFirstByUserIdAndIdTest() {
        Assert.assertNotNull(service.findFirstByUserIdAndId(UserUtil.getUserId(), alpha.getId()));
        @Nullable final Project test = service.findFirstByUserIdAndId(UserUtil.getUserId(), alpha.getId());
        Assert.assertNotNull(test);
        Assert.assertEquals(alpha.getId(), test.getId());
    }

    @Test
    public void deleteByUserIdTest() {
        service.clear(UserUtil.getUserId());
        Assert.assertEquals(0, service.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void removeTest() {
        @Nullable final List<Project> test = new ArrayList<>();
        test.add(alpha);
        test.add(beta);

        Assert.assertNotNull(test);
        service.remove(UserUtil.getUserId(), test);
        Assert.assertEquals(0, service.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void removeByByUserIdAndIdTest() {
        service.removeByByUserIdAndId(UserUtil.getUserId(), alpha.getId());
        Assert.assertNull(service.findFirstByUserIdAndId(UserUtil.getUserId(), alpha.getId()));
        Assert.assertEquals(1, service.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void removeByUserIdAndEntityTest() {
        service.removeByUserIdAndEntity(UserUtil.getUserId(), alpha);
        Assert.assertNull(service.findFirstByUserIdAndId(UserUtil.getUserId(), alpha.getId()));
        Assert.assertEquals(1, service.findAll(UserUtil.getUserId()).size());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(2, service.count(UserUtil.getUserId()));
    }

    @Test
    public void existsByIdTest() {
        Assert.assertTrue(service.existsById(UserUtil.getUserId(), alpha.getId()));
        Assert.assertFalse(service.existsById("123qe", alpha.getId()));
    }
}
